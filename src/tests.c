#include "tests.h"

#define ALLOCATION1_SIZE 6666
#define ALLOCATION2_SIZE 1488
#define ALLOCATION3_SIZE 322
#define ALLOCATION4_SIZE 2096
#define BIG_ALLOCATION 30000
#define OVERFLOW_ALLOCATION 20000
#define HEAP_SIZE 33322
#define SMALL_HEAP_SIZE 1


static void free_heap(void *heap, size_t size) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = size}).bytes);
}

//Обычное успешное выделение памяти
static void test_successful_allocation() {
    printf("_____________Test 1____________");
    void *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        err("Heap initialisation error\n");
        return;
    }
    debug_heap(stdout, heap);
    void *result = _malloc(ALLOCATION1_SIZE);
    if (!result) {
        err("Allocation error\n");
        return;
    }
    debug_heap(stdout, heap);
    printf("Successful allocation\n");
    _free(result);
    debug_heap(stdout, heap);
    free_heap(heap, HEAP_SIZE);
}

//Освобождение двух блоков из нескольких выделенных
static void test_freeing_several_blocks() {
    printf("_____________Test 2____________");
    void *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        err("Heap initialisation error\n");
        return;
    }
    debug_heap(stdout, heap);
    void *result1 = _malloc(ALLOCATION1_SIZE);
    void *result2 = _malloc(ALLOCATION2_SIZE);
    void *result3 = _malloc(ALLOCATION3_SIZE);
    void *result4 = _malloc(ALLOCATION4_SIZE);
    if (!result1 || !result2 || !result3 || !result4) {
        err("Allocation error\n");
        return;
    }
    debug_heap(stdout, heap);
    _free(result3);
    debug_heap(stdout, heap);
    _free(result2);
    debug_heap(stdout, heap);
    printf("Blocks deleted successfully\n");
    _free(result1);
    _free(result4);
    debug_heap(stdout, heap);
    free_heap(heap, HEAP_SIZE);
}

//Память закончилась, новый регион памяти расширяет старый
static void test_growing_heep() {
    printf("_______________Test 3______________");
    void *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        err("Heap initialisation error\n");
        return;
    }
    debug_heap(stdout, heap);
    void *result1 = _malloc(BIG_ALLOCATION);
    if (!result1) {
        err("Allocation error\n");
        return;
    }

    debug_heap(stdout, heap);
    void *result2 = _malloc(OVERFLOW_ALLOCATION);
    if (!result2) {
        err("Allocation error\n");
        return;
    }
    debug_heap(stdout, heap);
    printf("Heap extended successfully\n");
    _free(result2);
    _free(result1);
    debug_heap(stdout, heap);
    struct block_header* block = (struct block_header*) heap;
    free_heap(heap, size_from_capacity(block->capacity).bytes);
}

//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов,
//новый регион выделяется в другом месте
static void test_non_continuous_heap() {
    printf("_______________Test 4____________");
    void *heap = heap_init(SMALL_HEAP_SIZE  );
    if (!heap) {
        err("Heap initialisation error\n");
        return;
    }
    debug_heap(stdout, heap);

    printf("Successful allocation\n");
    (void) mmap(heap+REGION_MIN_SIZE, REGION_MIN_SIZE,
               PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);

    void *result = _malloc(BIG_ALLOCATION);
    if (!result) {
        err("Allocation error\n");
        return;
    }
    debug_heap(stdout, heap);
    _free(result);
    debug_heap(stdout, heap);
    munmap(heap, REGION_MIN_SIZE);
    free_heap(heap, 1);
}

void invoke_tests() {
    test_successful_allocation();
    test_freeing_several_blocks();
    test_growing_heep();
    test_non_continuous_heap();
}
